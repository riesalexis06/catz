package com.isys.catz.exception;

public class CatException extends Exception {
    private String errorMessage;

    public CatException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public CatException() {
        super();
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}