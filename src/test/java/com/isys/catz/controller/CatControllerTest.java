package com.isys.catz.controller;

import com.isys.catz.Application;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import org.springframework.http.MediaType;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@SpringBootTest
public class CatControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }

    @Test
    public void verifyCatList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/cats").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print());
    }

    @Test
    public void verifyCatExist() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/cats/Swinger").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Swinger"))
                .andExpect(jsonPath("$.color").value("BLACK_AND_WHITE"))
                .andDo(print());
    }

    @Test
    public void verifyCatNotExists() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/cats/UnknownCat").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode").value(404))
                .andExpect(jsonPath("$.message").value("Cat doesn´t exist"))
                .andDo(print());
    }

}
