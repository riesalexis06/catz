package com.isys.catz.controller;

import com.isys.catz.domain.Cat;
import com.isys.catz.domain.CatService;

import java.util.Collection;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
public class CatServiceTest {

    @InjectMocks
    public CatService service;

    @Test
    public void testFindByName() {
        Optional<Cat> snoozeCat =  service.findByName("Snooze");
        assertEquals("Snooze", snoozeCat.get().getName());

        Optional<Cat> swingerCat = service.findByName("Swinger");
        assertEquals("Swinger", swingerCat.get().getName());

        Optional<Cat> unknownCat = service.findByName("Unknown");
        assertFalse(unknownCat.isPresent());
    }

    @Test
    public void testFindAll() {
        Collection<Cat> cats = service.findAll();
        assertTrue(cats.size() == 2);
    }
}
