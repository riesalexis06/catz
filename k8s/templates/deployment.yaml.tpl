apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ APP_NAME }}-deploy
  labels:
    app: {{ APP_NAME }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{ APP_NAME }}
  template:
    metadata:
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/port: "8080"
        prometheus.io/path: "/actuator/prometheus"
      labels:
        app: {{ APP_NAME }}
        role: api
    spec:
      containers:
      - name: catz-api
        image: {{ IMAGE_TAG }}
        ports:
        - containerPort: 8080
          protocol: TCP
        resources:
          requests:
            memory: "256Mi"
            cpu: "80m"
          limits:
            memory: "512Mi"
            cpu: "250m"
        readinessProbe:
          exec:
            command:
              - sh
              - -c
              - >-
                curl --silent http://localhost:8080/actuator/health |
                grep --quiet -e '^{\"status\"\:\"UP\".*}$'
          initialDelaySeconds: 10
          periodSeconds: 5