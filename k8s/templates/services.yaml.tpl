apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ APP_NAME }}-ingress
  annotations:
    kubernetes.io/ingress.class: gce
{% if STATIC_IP_NAME is defined %}    kubernetes.io/ingress.global-static-ip-name: {{ STATIC_IP_NAME }}
{% endif %}
{% if DOMAIN_1 is defined %}    networking.gke.io/managed-certificates: {{ APP_NAME }}-certificate
{% endif %}
spec:
  defaultBackend:
    service:
      name: {{ APP_NAME }}-api
      port:
        number: 8080
---
apiVersion: cloud.google.com/v1
kind: BackendConfig
metadata:
  name: {{ APP_NAME }}-bc
spec:
  healthCheck:
    requestPath: /actuator/health
---
kind: Service
apiVersion: v1
metadata:
  name: {{ APP_NAME }}-api
  annotations:
    cloud.google.com/backend-config: '{"default": "{{ APP_NAME }}-bc"}'
spec:
  ports:
  - port: 8080
    name: api
  selector:
    app: {{ APP_NAME }}
    role: api
  type: NodePort