{% if DOMAIN_1 is defined %}
apiVersion: networking.gke.io/v1
kind: ManagedCertificate
metadata:
  name: {{ APP_NAME }}-certificate
spec:
  domains:
{% for key, value in environment('DOMAIN_') %}  - {{ value }}{% endfor %}
{% endif %}