FROM gradle:5.6.4-jdk11 AS builder
WORKDIR /app-build
COPY . .
RUN gradle clean build

FROM openjdk:11
WORKDIR /app
COPY --from=0 /app-build/build/libs/isys-catz.jar .
CMD java -jar ./isys-catz.jar