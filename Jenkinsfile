pipeline {
    agent {
        kubernetes {
            label 'isys-catz-build'
            yamlFile 'k8s/build-pod.yaml'
            defaultContainer 'jnlp'
        }
    }

    environment {
        PROJECT = "instant-system-316010"                       //nom du projet Google Cloud
        APP_NAME = "isys-catz"                                  //nom de l'application
        FE_SVC_NAME = "${APP_NAME}-api"                         //nom du service
        IMAGE_TAG = "gcr.io/${PROJECT}/${APP_NAME}:${BUILD_ID}" //tag de l'image Docker
        CLUSTER_STAGING = "instantsystem"                       //nom du cluster k8s de preproduction
        CLUSTER_PROD = "instantsystem-prod"                     //nom du cluster k8s de production
        CLUSTER_ZONE = "europe-west1-d"                         //zone de disponibilité des clusters
        JENKINS_CRED = "${PROJECT}"                             //nom du credential Jenkins pour le déploiement sur k8s
        PROD_DOMAIN = "catz.rcloud.fr"                          //nom de domaine de production
        STAGING_DOMAIN = "staging.catz.rcloud.fr"               //nom de domaine de preproduction
    }

    stages {
        stage("Test & Quality check") {
            parallel {
                stage ('Tests') {
                    steps {
                        container('gradle') {
                            sh 'gradle test'
                        }
                    }
                }
                stage("QA") {
                    stages {
                        stage("SonarQube analysis") {
                            steps {
                                container('gradle') {
                                    withSonarQubeEnv('isys-sonarqube') {
                                        sh "gradle sonarqube --info"
                                    }
                                }
                            }
                        }
                        stage("Wait for SonarQube quality gate") {
                            steps {
                                timeout(time: 1, unit: 'HOURS') {
                                    waitForQualityGate abortPipeline: true
                                }
                            }
                        }
                    }
                }
            }
        }

        stage('Build and push image with Container Builder') {
            when {
                branch 'master'
            }
            steps {
                container('gcloud') {
                    sh "PYTHONUNBUFFERED=1 gcloud builds submit -t ${IMAGE_TAG} ."
                }
            }
        }

        stage('Deploy staging') {
            when {
                branch 'master'
            }
            environment {
                DOMAIN_1 = "${STAGING_DOMAIN}"
                NAMESPACE = "${APP_NAME}-staging"
                CLUSTER = "${CLUSTER_STAGING}"
                STATIC_IP_NAME = "${APP_NAME}-staging"
            }
            steps {
                container('envtpl') {
                    sh("for f in k8s/templates/*.yaml.tpl; do echo \"Processing \$f file..\"; envtpl --keep-template --allow-missing \$f; done")
                    sh("mkdir -p k8s/deploy; mv k8s/templates/*.yaml k8s/deploy/")
                    sh("chmod 666 k8s/deploy/*.yaml")
                }
                container('kubectl') {
                    step([$class: 'KubernetesEngineBuilder', namespace: env.NAMESPACE, projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/deploy/', credentialsId: env.JENKINS_CRED, verifyDeployments: true])
                    sh("echo http://`kubectl --namespace=${env.NAMESPACE} get service/${env.FE_SVC_NAME} -o jsonpath='{.status.loadBalancer.ingress[0].ip}'` > ${env.FE_SVC_NAME}")
                }
            }
        }

        stage('Wait approval') {
            when {
                branch 'master'
            }
            steps {
                timeout(time: 7, unit: 'DAYS') {
                    input message: 'Do you want to deploy to prod ?', submitter: 'ops'
                }
            }
        }

        stage('Deploy production') {
            when {
                branch 'master'
            }
            environment {
                DOMAIN_1 = "${PROD_DOMAIN}"
                NAMESPACE = "${APP_NAME}-prod"
                CLUSTER = "${CLUSTER_PROD}"
                STATIC_IP_NAME = "${APP_NAME}-prod"
            }
            steps {
                container('envtpl') {
                    sh("for f in k8s/templates/*.yaml.tpl; do echo \"Processing \$f file..\"; envtpl --keep-template --allow-missing \$f; done")
                    sh("mkdir -p k8s/deploy; mv k8s/templates/*.yaml k8s/deploy/")
                    sh("chmod 666 k8s/deploy/*.yaml")
                }
                container('kubectl') {
                    step([$class: 'KubernetesEngineBuilder', namespace: env.NAMESPACE, projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/deploy/', credentialsId: env.JENKINS_CRED, verifyDeployments: true])
                    sh("echo http://`kubectl --namespace=${env.NAMESPACE} get service/${env.FE_SVC_NAME} -o jsonpath='{.status.loadBalancer.ingress[0].ip}'` > ${env.FE_SVC_NAME}")
                }
            }
        }
    }
}
